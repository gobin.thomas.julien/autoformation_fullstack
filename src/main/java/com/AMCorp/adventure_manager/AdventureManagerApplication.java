package com.AMCorp.adventure_manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdventureManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdventureManagerApplication.class, args);
	}

}
