package com.AMCorp.adventure_manager.adventure.repositories;

import com.AMCorp.adventure_manager.adventure.repositories.entities.AdventureEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdventureRepository extends CrudRepository<AdventureEntity, Long> {
    List<AdventureEntity> findByName(String name);
}
