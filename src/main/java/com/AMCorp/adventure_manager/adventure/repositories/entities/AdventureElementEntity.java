package com.AMCorp.adventure_manager.adventure.repositories.entities;

import jakarta.persistence.*;
import lombok.*;

@Data
@Entity(name = "am_adventure_element")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@IdClass(AdventureElementEntity.Key.class)
public class AdventureElementEntity {
    @Id
    @GeneratedValue
    private Long id;

    @Id
    @ManyToOne
    private AdventureEntity adventure;

    private String name;
    private String description;
    private String imgUrl;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    public static class Key {
        private Long id;
        private Long adventure;
    }
}
