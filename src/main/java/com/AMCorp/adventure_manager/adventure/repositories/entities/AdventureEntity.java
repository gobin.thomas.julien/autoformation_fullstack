package com.AMCorp.adventure_manager.adventure.repositories.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity(name = "am_adventure")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdventureEntity {
    @Id
    @GeneratedValue
    private Long id;
    @Column(unique=true)
    private String name;
}
