package com.AMCorp.adventure_manager.adventure.repositories;

import com.AMCorp.adventure_manager.adventure.repositories.entities.AdventureElementEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdventureElementRepository extends CrudRepository<AdventureElementEntity, AdventureElementEntity.Key> {

    List<AdventureElementEntity> findByAdventureId(Long adventureId);

    void deleteByAdventureId(Long adventureId);
}
