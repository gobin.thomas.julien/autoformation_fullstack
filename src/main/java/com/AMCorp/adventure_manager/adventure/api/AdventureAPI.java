package com.AMCorp.adventure_manager.adventure.api;

import com.AMCorp.adventure_manager.adventure.api.dtos.in.AdventureInDTO;
import com.AMCorp.adventure_manager.adventure.api.dtos.out.AdventureOutDTO;
import com.AMCorp.adventure_manager.adventure.services.AdventureService;
import com.AMCorp.adventure_manager.adventure.services.beans.Adventure;
import com.AMCorp.adventure_manager.adventure.services.exception.AdventureException;
import com.AMCorp.adventure_manager.adventure.services.exception.AdventureNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController()
@RequestMapping("adventures")
public class AdventureAPI {

    @Autowired
    private AdventureService service;

    @PostMapping(value = "")
    @Transactional
    public ResponseEntity<AdventureOutDTO> processAdventure(@RequestBody AdventureInDTO adventureDTO) throws AdventureException {
        if (adventureDTO.isDelete()) {
            service.delete(adventureDTO.getId());
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.ok(beanToDTO(service.createOrUpdate(dtoToBean(adventureDTO))));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<AdventureOutDTO> getAdventure(@PathVariable Long id) throws AdventureNotFoundException {
        return ResponseEntity.ok(beanToDTO(service.get(id)));
    }

    @DeleteMapping(value = "/{id}")
    @Transactional
    public ResponseEntity<String> deleteAdventure(@PathVariable Long id) {
        service.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "")
    public ResponseEntity<List<AdventureOutDTO>> findAdventures() {
        return ResponseEntity.ok(service.find().stream().map(this::beanToDTO).collect(Collectors.toList()));
    }

    public AdventureOutDTO beanToDTO(Adventure bean) {
        return AdventureOutDTO.builder()
                .id(bean.getId())
                .name(bean.getName())
                .build();
    }

    public Adventure dtoToBean(AdventureInDTO dto) {
        return Adventure.builder()
                .id(dto.getId())
                .name(dto.getName())
                .build();
    }
}
