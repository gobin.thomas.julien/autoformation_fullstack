package com.AMCorp.adventure_manager.adventure.api.dtos.out;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdventureOutDTO {
    private Long id;
    private String name;

    private String error;
}
