package com.AMCorp.adventure_manager.adventure.api.dtos.out;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdventureElementOutDTO {
    private Long id;
    private String name;
    private String description;
    private String imgUrl;

    private Long adventureId;
    private String adventureName;

    private String error;
}
