package com.AMCorp.adventure_manager.adventure.api;

import com.AMCorp.adventure_manager.adventure.api.dtos.in.AdventureElementInDTO;
import com.AMCorp.adventure_manager.adventure.api.dtos.out.AdventureElementOutDTO;
import com.AMCorp.adventure_manager.adventure.services.AdventureElementService;
import com.AMCorp.adventure_manager.adventure.services.AdventureService;
import com.AMCorp.adventure_manager.adventure.services.beans.Adventure;
import com.AMCorp.adventure_manager.adventure.services.beans.AdventureElement;
import com.AMCorp.adventure_manager.adventure.services.exception.AdventureElementException;
import com.AMCorp.adventure_manager.adventure.services.exception.AdventureElementNotFoundException;
import com.AMCorp.adventure_manager.adventure.services.exception.AdventureNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("")
public class AdventureElementAPI {

    @Autowired
    private AdventureElementService elementService;

    @Autowired
    private AdventureService adventureService;

    @GetMapping(value = "/elements")
    public ResponseEntity<List<AdventureElementOutDTO>> findAdventureElements() {
        return ResponseEntity.ok(elementService.find().stream().map(this::beanToDTO).collect(Collectors.toList()));
    }

    @GetMapping(value = "/adventures/{adventureNameOrId}/elements")
    public ResponseEntity<List<AdventureElementOutDTO>> findAdventureElements(@PathVariable String adventureNameOrId) throws AdventureNotFoundException {
        Adventure adventure = adventureService.getByNameOrId(adventureNameOrId);
        return ResponseEntity.ok(elementService.find(adventure.getId()).stream().map(this::beanToDTO).collect(Collectors.toList()));
    }

    @PostMapping(value = "/adventures/{adventureNameOrId}/elements")
    @Transactional
    public ResponseEntity<List<AdventureElementOutDTO>> processAdventureElement(@PathVariable String adventureNameOrId, @RequestBody List<AdventureElementInDTO> elementDTOs) throws AdventureNotFoundException {
        Adventure adventure = adventureService.getByNameOrId(adventureNameOrId);

        List<AdventureElementOutDTO> outDTOs = new ArrayList<>();
        ResponseEntity.BodyBuilder responseBuilder = ResponseEntity.ok();

        for (AdventureElementInDTO dto : elementDTOs.stream().filter(AdventureElementInDTO::isDelete).toList()) {
            elementService.delete(adventure.getId(), dto.getId());
        }

        for (AdventureElementInDTO dto : elementDTOs.stream().filter(dto -> !dto.isDelete()).toList()) {
            AdventureElement element = dtoToBean(adventure, dto);
            try {
                outDTOs.add(beanToDTO(elementService.createOrUpdate(element)));
            }
            catch (Exception ex) {
                responseBuilder = ResponseEntity.internalServerError();
                outDTOs.add(errorDTO(element, ex.getMessage()));
            }
        }

        return responseBuilder.body(outDTOs);
    }

    @GetMapping(value = "/adventures/{adventureNameOrId}/elements/{id}")
    public ResponseEntity<AdventureElementOutDTO> getAdventureElement(@PathVariable String adventureNameOrId, @PathVariable Long id) throws AdventureNotFoundException, AdventureElementNotFoundException {
        Adventure adventure = adventureService.getByNameOrId(adventureNameOrId);
        return ResponseEntity.ok(beanToDTO(elementService.get(adventure.getId(), id)));
    }

    @DeleteMapping(value = "/adventures/{adventureNameOrId}/elements/{id}")
    @Transactional
    public ResponseEntity<AdventureElementOutDTO> deleteAdventureElement(@PathVariable String adventureNameOrId, @PathVariable Long id) throws AdventureNotFoundException {
        Adventure adventure = adventureService.getByNameOrId(adventureNameOrId);
        elementService.delete(adventure.getId(), id);
        return ResponseEntity.ok().build();
    }

    public AdventureElementOutDTO beanToDTO(AdventureElement bean) {
        var builder = AdventureElementOutDTO.builder()
                .id(bean.getId())
                .name(bean.getName())
                .description(bean.getDescription())
                .imgUrl(bean.getImgUrl());

        if (bean.getAdventure() != null) {
            builder.adventureId(bean.getAdventure().getId())
                    .adventureName(bean.getAdventure().getName());
        }

        return builder.build();
    }

    public AdventureElement dtoToBean(Adventure adventure, AdventureElementInDTO dto) {
        return AdventureElement.builder()
                .id(dto.getId())
                .name(dto.getName())
                .description(dto.getDescription())
                .imgUrl(dto.getImgUrl())
                .adventure(adventure)
                .build();
    }

    private AdventureElementOutDTO errorDTO(AdventureElement bean, String errorMessage) {
        AdventureElementOutDTO dto = beanToDTO(bean);
        dto.setError(errorMessage);
        return dto;
    }
}
