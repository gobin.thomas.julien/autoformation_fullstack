package com.AMCorp.adventure_manager.adventure.api.exception;

import com.AMCorp.adventure_manager.adventure.services.exception.AdventureElementNotFoundException;
import com.AMCorp.adventure_manager.adventure.services.exception.AdventureNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class AdventureExceptionHandler {

    @ExceptionHandler(value = {AdventureNotFoundException.class, AdventureElementNotFoundException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String adventureNotFound(Exception ex, WebRequest request) {
        return ex.getMessage();
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public String serverError(Exception ex, WebRequest request) {
        return ex.getMessage();
    }
}