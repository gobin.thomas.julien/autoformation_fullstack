package com.AMCorp.adventure_manager.adventure.services.beans;

import lombok.Builder;
import lombok.Data;
@Data
@Builder
public class Adventure {
    private Long id;
    private String name;
}
