package com.AMCorp.adventure_manager.adventure.services.exception;

public class AdventureNotFoundException extends AdventureException {
    public AdventureNotFoundException(Long id) {
        super (String.format("Adventure %s does not exists", id), null);
    }

    public AdventureNotFoundException(String nameOrId) {
        super (String.format("Adventure %s does not exists", nameOrId), null);
    }
}
