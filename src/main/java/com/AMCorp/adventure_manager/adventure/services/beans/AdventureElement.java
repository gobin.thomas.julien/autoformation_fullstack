package com.AMCorp.adventure_manager.adventure.services.beans;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AdventureElement {
    private Long id;

    private String name;
    private String description;
    private String imgUrl;

    private Adventure adventure;
}
