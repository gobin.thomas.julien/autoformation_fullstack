package com.AMCorp.adventure_manager.adventure.services;

import com.AMCorp.adventure_manager.adventure.repositories.AdventureRepository;
import com.AMCorp.adventure_manager.adventure.repositories.entities.AdventureEntity;
import com.AMCorp.adventure_manager.adventure.services.beans.Adventure;
import com.AMCorp.adventure_manager.adventure.services.exception.AdventureException;
import com.AMCorp.adventure_manager.adventure.services.exception.AdventureNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AdventureService {

    @Autowired
    private AdventureRepository repository;

    @Autowired
    private AdventureElementService elementService;

    public Adventure createOrUpdate(Adventure bean) throws AdventureException {
        if (bean.getId() != null && repository.existsById(bean.getId())) {
            return update(bean);
        }
        return create(bean);
    }

    public Adventure update(Adventure bean) throws AdventureException {
        try {
            Optional<AdventureEntity> dbEntity = repository.findById(bean.getId());
            if (dbEntity.isPresent()) {
                AdventureEntity entity = dbEntity.get();
                entity.setName(bean.getName());
                return entityToBean(repository.save(entity));
            }
            throw new AdventureNotFoundException(bean.getId());
        }
        catch (Exception ex) {
            throw new AdventureException(String.format("Error when updating adventure (%s) %s : %s", bean.getId(), bean.getName(), ex.getMessage()), ex);
        }
    }
    public Adventure create(Adventure adventure) throws AdventureException {
        try {
            return entityToBean(repository.save(beanToEntity(adventure)));
        }
        catch (Exception ex) {
            throw new AdventureException(String.format("Error when creating new adventure %s : %s", adventure.getName(), ex.getMessage()), ex);
        }
    }

    public List<Adventure> find() {
        List<Adventure> beans = new ArrayList<>();
        repository.findAll().forEach(entity -> beans.add(entityToBean(entity)));
        return beans;
    }

    public Adventure getByNameOrId(String nameOrId) throws AdventureNotFoundException {
        List<AdventureEntity> dbEntity = repository.findByName(nameOrId);
        if (dbEntity.size() > 0) {
            return entityToBean(dbEntity.get(0));
        }

        try {
            return this.get(Long.parseLong(nameOrId));
        }
        catch (NumberFormatException ex) {
            throw new AdventureNotFoundException(nameOrId);
        }
    }

    public Adventure get(Long id) throws AdventureNotFoundException {
        return repository.findById(id).map(AdventureService::entityToBean).orElseThrow(() -> new AdventureNotFoundException(id));
    }

    public void delete(Long id) {
        elementService.deleteAllForAdventure(id);
        repository.deleteById(id);
    }

    public static Adventure entityToBean(AdventureEntity entity) {
        return Adventure.builder()
                .id(entity.getId())
                .name(entity.getName())
                .build();
    }

    public static AdventureEntity beanToEntity(Adventure bean) {
        return AdventureEntity.builder()
                .id(bean.getId())
                .name(bean.getName())
                .build();
    }
}
