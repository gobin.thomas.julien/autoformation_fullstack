package com.AMCorp.adventure_manager.adventure.services.exception;

import com.AMCorp.adventure_manager.adventure.repositories.entities.AdventureElementEntity;

public class AdventureElementNotFoundException extends AdventureElementException {
    public AdventureElementNotFoundException(AdventureElementEntity.Key key) {
        super(String.format("Element %s does not exists", key), null);
    }
}
