package com.AMCorp.adventure_manager.adventure.services;

import com.AMCorp.adventure_manager.adventure.repositories.AdventureElementRepository;
import com.AMCorp.adventure_manager.adventure.repositories.entities.AdventureElementEntity;
import com.AMCorp.adventure_manager.adventure.repositories.entities.AdventureEntity;
import com.AMCorp.adventure_manager.adventure.services.beans.AdventureElement;
import com.AMCorp.adventure_manager.adventure.services.exception.AdventureElementException;
import com.AMCorp.adventure_manager.adventure.services.exception.AdventureElementNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Log4j2
public class AdventureElementService {

    @Autowired
    private AdventureElementRepository repository;

    public AdventureElement createOrUpdate(AdventureElement bean) throws AdventureElementException {
        if (bean.getId() != null && repository.existsById(beanToEntityKey(bean))) {
            return update(bean);
        }
        return create(bean);
    }

    public AdventureElement update(AdventureElement bean) throws AdventureElementException {
        AdventureElementEntity.Key key = beanToEntityKey(bean);
        try {
            Optional<AdventureElementEntity> dbEntity = repository.findById(key);
            if (dbEntity.isPresent()) {
                AdventureElementEntity entity = dbEntity.get();
                entity.setName(bean.getName());
                entity.setImgUrl(bean.getImgUrl());
                entity.setDescription(bean.getDescription());
                return entityToBean(repository.save(entity));
            }
            throw new AdventureElementNotFoundException(key);
        }
        catch (Exception ex) {
            throw new AdventureElementException(String.format("Error when updating element %s : %s", key, ex.getMessage()), ex);
        }
    }
    public AdventureElement create(AdventureElement bean) throws AdventureElementException {
        try {
            return entityToBean(repository.save(beanToEntity(bean)));
        }
        catch (Exception ex) {
            throw new AdventureElementException(String.format("Error when creating new element %s : %s", beanToEntityKey(bean), ex.getMessage()), ex);
        }
    }

    public List<AdventureElement> find() {
        List<AdventureElement> adventures = new ArrayList<>();
        repository.findAll().forEach(entity -> adventures.add(entityToBean(entity)));
        return adventures;
    }

    public List<AdventureElement> find(Long adventureId) {
        List<AdventureElement> adventures = new ArrayList<>();
        repository.findByAdventureId(adventureId).forEach(entity -> adventures.add(entityToBean(entity)));
        return adventures;
    }

    public AdventureElement get(Long adventureId, Long id) throws AdventureElementNotFoundException {
        AdventureElementEntity.Key key = AdventureElementEntity.Key.builder()
                .adventure(adventureId)
                .id(id)
                .build();

        return repository.findById(key)
                .map(AdventureElementService::entityToBean)
                .orElseThrow(() -> new AdventureElementNotFoundException(key));
    }

    public void delete(Long adventureId, Long id) {
        AdventureElementEntity.Key key = AdventureElementEntity.Key.builder()
                .adventure(adventureId)
                .id(id)
                .build();

        repository.deleteById(key);
    }

    public void deleteAllForAdventure(Long adventureId) {
        repository.deleteByAdventureId(adventureId);
    }

    public static AdventureElement entityToBean(AdventureElementEntity entity) {
        return AdventureElement.builder()
                .id(entity.getId())
                .name(entity.getName())
                .imgUrl(entity.getImgUrl())
                .adventure(AdventureService.entityToBean(entity.getAdventure()))
                .description(entity.getDescription())
                .build();
    }

    public static AdventureElementEntity beanToEntity(AdventureElement bean) {
        return AdventureElementEntity.builder()
                .id(bean.getId())
                .name(bean.getName())
                .imgUrl(bean.getImgUrl())
                .description(bean.getDescription())
                .adventure(AdventureEntity.builder().id(bean.getAdventure().getId()).build())
                .build();
    }

    public static AdventureElementEntity.Key beanToEntityKey(AdventureElement bean) {
        return AdventureElementEntity.Key.builder()
                .id(bean.getId())
                .adventure(bean.getAdventure().getId())
                .build();
    }
}
