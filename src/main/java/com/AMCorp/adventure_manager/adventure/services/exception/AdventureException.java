package com.AMCorp.adventure_manager.adventure.services.exception;

public class AdventureException extends Exception {
    public AdventureException(String message, Throwable cause) {
        super(message, cause);
    }
}
