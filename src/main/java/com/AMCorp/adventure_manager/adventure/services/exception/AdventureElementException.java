package com.AMCorp.adventure_manager.adventure.services.exception;

public class AdventureElementException extends Exception {
    public AdventureElementException(String message, Throwable cause) {
        super(message, cause);
    }
}
